window.onload = function() {

    let canvas = document.getElementById("canvas"),c = canvas.getContext("2d");

    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    w = window.innerWidth,h = window.innerHeight;
    c.fillStyle = "rgba(30,30,30,1)";
    c.fillRect(0, 0, w, h);

    function cc(A,B){
      let min = 100,max = 1000,
        rx = Math.floor(Math.random()*(max-min+1)+min),
        ry = Math.floor(Math.random()*(max-min+1)+min);
      let S={x:rx,y:ry}
      return S;
    }

    class point{
      //此段是js的构造函数，只用于学习
        constructor(x,y){
          this.x = x || Math.random()*w;//其实没有传递x，则随机数乘以宽度
          this.y = y || Math.random()*h;//其实没有传递y，则随机数乘以高度
        }
        draw_point_xy(){
            for(let i = 0; i < 100; i++){
              c.globalCompositeOperation="lighter";
              c.beginPath();
              for(let j = i; j < 100; j++){
                this.s = cc();//生成100个随机点位置
                c.fillStyle="green";
                c.lineTo(this.s.x,this.s.y);
              }
              c.strokeStyle="green";//线条色彩
              c.lineWidth=0.05;//线条宽度
              c.stroke();//闭合
              c.globalCompositeOperation="source-over";//全局模式
          }
        }
    }
    function draw() {
        c.fillStyle = "rgba(30,30,30,1)";//绘制黑色背景
        c.fillRect(0, 0, w, h);//填充矩形
        // p[p.length-1].draw_point_xy(p);//实现绘制
        //这就是一段有问题的设计方式
        new point().draw_point_xy();
    }
    draw();//这里是黑色背景的代码

    //动画函数
    window.requestAnimFrame = (function() {
      return (
          window.requestAnimationFrame ||
          window.webkitRequestAnimationFrame ||
          window.mozRequestAnimationFrame ||
          window.oRequestAnimationFrame ||
          window.msRequestAnimationFrame ||
          function(callback) {
            window.setTimeout(callback);
          }
      );
  });
}